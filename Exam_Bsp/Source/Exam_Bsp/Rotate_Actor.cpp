// Fill out your copyright notice in the Description page of Project Settings.


#include "Rotate_Actor.h"

// Sets default values
ARotate_Actor::ARotate_Actor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryActorTick.bCanEverTick = true;
	PitchValue = 0.f;
	YawValue = 0.f;
	RollValue = 0.f;
	// ...
}

// Called when the game starts or when spawned
void ARotate_Actor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ARotate_Actor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	FQuat QuatRotation = FQuat(FRotator(PitchValue, YawValue, RollValue));
	AddActorLocalRotation(QuatRotation, false, 0, ETeleportType::None);
}

