// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef EXAM_BSP_Rotate_Actor_generated_h
#error "Rotate_Actor.generated.h already included, missing '#pragma once' in Rotate_Actor.h"
#endif
#define EXAM_BSP_Rotate_Actor_generated_h

#define Exam_Bsp_Source_Exam_Bsp_Rotate_Actor_h_12_SPARSE_DATA
#define Exam_Bsp_Source_Exam_Bsp_Rotate_Actor_h_12_RPC_WRAPPERS
#define Exam_Bsp_Source_Exam_Bsp_Rotate_Actor_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Exam_Bsp_Source_Exam_Bsp_Rotate_Actor_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARotate_Actor(); \
	friend struct Z_Construct_UClass_ARotate_Actor_Statics; \
public: \
	DECLARE_CLASS(ARotate_Actor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Exam_Bsp"), NO_API) \
	DECLARE_SERIALIZER(ARotate_Actor)


#define Exam_Bsp_Source_Exam_Bsp_Rotate_Actor_h_12_INCLASS \
private: \
	static void StaticRegisterNativesARotate_Actor(); \
	friend struct Z_Construct_UClass_ARotate_Actor_Statics; \
public: \
	DECLARE_CLASS(ARotate_Actor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Exam_Bsp"), NO_API) \
	DECLARE_SERIALIZER(ARotate_Actor)


#define Exam_Bsp_Source_Exam_Bsp_Rotate_Actor_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARotate_Actor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARotate_Actor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARotate_Actor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARotate_Actor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARotate_Actor(ARotate_Actor&&); \
	NO_API ARotate_Actor(const ARotate_Actor&); \
public:


#define Exam_Bsp_Source_Exam_Bsp_Rotate_Actor_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARotate_Actor(ARotate_Actor&&); \
	NO_API ARotate_Actor(const ARotate_Actor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARotate_Actor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARotate_Actor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ARotate_Actor)


#define Exam_Bsp_Source_Exam_Bsp_Rotate_Actor_h_12_PRIVATE_PROPERTY_OFFSET
#define Exam_Bsp_Source_Exam_Bsp_Rotate_Actor_h_9_PROLOG
#define Exam_Bsp_Source_Exam_Bsp_Rotate_Actor_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Exam_Bsp_Source_Exam_Bsp_Rotate_Actor_h_12_PRIVATE_PROPERTY_OFFSET \
	Exam_Bsp_Source_Exam_Bsp_Rotate_Actor_h_12_SPARSE_DATA \
	Exam_Bsp_Source_Exam_Bsp_Rotate_Actor_h_12_RPC_WRAPPERS \
	Exam_Bsp_Source_Exam_Bsp_Rotate_Actor_h_12_INCLASS \
	Exam_Bsp_Source_Exam_Bsp_Rotate_Actor_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Exam_Bsp_Source_Exam_Bsp_Rotate_Actor_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Exam_Bsp_Source_Exam_Bsp_Rotate_Actor_h_12_PRIVATE_PROPERTY_OFFSET \
	Exam_Bsp_Source_Exam_Bsp_Rotate_Actor_h_12_SPARSE_DATA \
	Exam_Bsp_Source_Exam_Bsp_Rotate_Actor_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Exam_Bsp_Source_Exam_Bsp_Rotate_Actor_h_12_INCLASS_NO_PURE_DECLS \
	Exam_Bsp_Source_Exam_Bsp_Rotate_Actor_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> EXAM_BSP_API UClass* StaticClass<class ARotate_Actor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Exam_Bsp_Source_Exam_Bsp_Rotate_Actor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
